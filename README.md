**Kompilacja klienta w Eclipse**

Otworzyć Eclipse.
W zakładce File wybrać Import/General/Projects from Folder or Archive.
Wybrać Next.
Wybrać Directory i wskazać folder z programem.
Wybrać Finish.

Wybrać Run as/Java Application.
W razie potrzeby wybrać jako główną klasę klasę Main.

**Kompilacja serwera w Visual Studio 2014**

Wybrać plik projektsk/graServer.sln.
Wybrać Debug/Start debugging

**Kompilacja serwera linuxowego**

Otworzyć terminal w folderze zawierającym plik makefile.
Wywołać polecenie make.

**Włączenie klienta**

W linii poleceń: java -jar GraKlient.jar

**Włączenie serwera na Windowsie**

Uruchomienie pliku projektsk\graServer\x64\Debug\graSerwer.exe

**Włączenie serwera na Linuxie**

Uruchomienie pliku graServer.

**Protokół komunikacji**

Serwer po uruchomieniu tworzy kolejkę dla 5 klientów i oczekuje na nich.
Po uzyskaniu połączenia, klient wysyła do serwera wiadomość zawierającą nazwę wybraną przez użytkownika ("AlaMakota\n"); jeżeli nie została ona zajęta serwer wysyła wiadomość potwierdzającą ("OK\n") oraz rozsyła tą nazwę do wszystkich pozostałych graczy, a ich nazwy - do klienta. W przypadku nieotrzymania tej wiadomości - klient czeka, aż nie nastąpi timeout socketa.

Komunikaty kończą się znakiem nowej linii. 

Jeżeli klient zostanie zaakceptowany może wysyłać polecenia odnośnie tworzenia jednostek lub ataku innych ("1,0,0,0,1,AlaMakota" - zaatakuj AlaMakota; "2,1,0,0,0,-" - wyszkol jednego robotnika). Pola komunikatów klienta przedzielane są przecinkiem, a na rozkaz składają się kolejno: typ rozkazu, cztery liczby przedstawiające ile jednostek ów rozkaz dotyczy,nazwa przeciwnika lub puste pole.

Serwer, w przypadku zajścia zmiany stanu posiadania gracza (nowe jednostki, więcej zasobów etc.) wysyła mu komunikat z nowym stanem ("1 300 0 0 0 1" 1 punkt, 300 jednostek zasobów, kolejne pola to liczność przebywających w mieście jednostek). Pola komunikatów serwer przedzialone są spacją lub są to komunikaty, które klient ma wyświetlić ("Oszust! Nie masz tylu jednostek!")(Da się je odróżnić po liczbie spacji).

W przypadku wyłączenia się klienta, błąd odczytu jet sygnałem dla serwera do usunięcia jego danych. W przypadku wyłączenia się serwera klient resetuje się do okienka "logowania".

**Opis plików źródłowych**

***Serwer***: 

1. Jednostka - Charakterystyka jednostki

2. Logika - funkcje odpowiedzialne za atak i szkolenie nowych jednostek

3. Stan - stan miasta (zasoby, jednostki, punkty etc.)

4. Miasto - klasa opakowująca i obsługująca stan (zmiana wartości pól)

5. Main - komunikacja z klientami

***Klient***:

1. Main - uruchomienie klienta

2. TheFrame - główne okienko - logowanie, komunikacja z serwerem, otwieranie pomocniczych okienek

3. AtakFrame - zebranie danych koniecznych do wysłania komunikatu "Atak"

4. SzkolenieFrame - zebranie danych koniecznych do wysłania komunikatu "Szkolenie"

5. StanFrame - wyświetlenie stanu gry w danym momencie