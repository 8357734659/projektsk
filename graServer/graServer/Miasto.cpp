#include "Miasto.h"

unsigned int __stdcall tmp(void* data) {
	((Miasto*) data)->wydobycie();
	return 0;
}

void Miasto::wydobycie()
{
	int ile = 50;
	DWORD co_ile = 5000;
	int wplyw = 5;
	while (stan->t) {
		WaitForSingleObject(using_s, INFINITE);
		stan->surowce += (ile + wplyw * stan->robotnicy);
		zmiana = true;
		ReleaseMutex(using_s);
		Sleep(co_ile);
	}
}

void Miasto::killall()
{
	this->lock();
	stan->c_piechota = 0;
	stan->l_piechota = 0;
	stan->jazda = 0;
	zmiana = true;
	this->unlock();
}

Miasto::Miasto(std::string nazwa)
{
	this->stan = new Stan(nazwa);
	this->h = (HANDLE)_beginthreadex(0, 0, &tmp, this, 0, 0);
	SECURITY_ATTRIBUTES security = {
		sizeof(security), nullptr, /* bInheritHandle = */ TRUE
	};
	this->using_s = CreateMutex(&security, FALSE, NULL);
	this->using_j = CreateMutex(&security, FALSE, NULL);
	this->using_l = CreateMutex(&security, FALSE, NULL);
	this->using_c = CreateMutex(&security, FALSE, NULL);
}

Miasto::~Miasto()
{
	this->stan->t = false;
	WaitForSingleObject(h, INFINITE);
	CloseHandle(h);
	CloseHandle(using_s);
	CloseHandle(using_j);
	CloseHandle(using_l);
	CloseHandle(using_c);
	delete stan;
}

double Miasto::sila_ataku(Jednostka *j, Jednostka *l, Jednostka *c,int lj, int ll , int lc)
{
	return lj*j->Atak + ll*l->Atak + lc*c->Atak;
}

double Miasto::sila_ataku(Jednostka *j, Jednostka *l, Jednostka *c)
{
	return stan->jazda*j->Atak + stan->l_piechota*l->Atak + stan->c_piechota*c->Atak;
}

double Miasto::sila_obrony(Jednostka *j, Jednostka *l, Jednostka *c, int lj, int ll, int lc)
{
	return lj*j->Obrona + ll*l->Obrona + lc*c->Obrona;
}

double Miasto::sila_obrony(Jednostka *j, Jednostka *l, Jednostka *c)
{
	return stan->jazda*j->Obrona + stan->l_piechota*l->Obrona + stan->c_piechota*c->Obrona;
}

int Miasto::treningR(int ile, Jednostka *x)
{//mutex
	WaitForSingleObject(using_s, INFINITE);
	if (ile >= 0 && ile*x->Cena<= stan->surowce) {
		stan->surowce -= x->Cena*ile;
		zmiana = true;
		ReleaseMutex(using_s);
		Sleep(x->Czas);
		stan->robotnicy += ile;
		zmiana = true;
	}
	else {
		ReleaseMutex(using_s);
		return -1;
	}
	return		0;
}

int Miasto::treningL(int ile, Jednostka *x)
{
	WaitForSingleObject(using_s, INFINITE);
	if (ile >= 0 && ile*x->Cena <= stan->surowce) {
		stan->surowce -= x->Cena*ile;
		zmiana = true;
		ReleaseMutex(using_s);
		
		Sleep(x->Czas);

		WaitForSingleObject(using_l, INFINITE);
		stan->l_piechota += ile;
		zmiana = true;
		ReleaseMutex(using_l);
	}
	else {
		ReleaseMutex(using_s);
		return -1;
	}
	return		0;
}

int Miasto::treningC(int ile, Jednostka *x)
{//mutex
	WaitForSingleObject(using_s, INFINITE);
	if (ile >= 0 && ile*x->Cena <= stan->surowce) {
		stan->surowce -= x->Cena*ile;
		zmiana = true;
		ReleaseMutex(using_s);

		Sleep(x->Czas);

		WaitForSingleObject(using_c, INFINITE);
		stan->c_piechota += ile;
		zmiana = true;
		ReleaseMutex(using_c);
	}
	else {
		ReleaseMutex(using_s);
		return -1;
	}
	return		0;
}

int Miasto::treningJ(int ile, Jednostka *x)
{//mutex
	WaitForSingleObject(using_s, INFINITE);
	if (ile >= 0 && ile*x->Cena <= stan->surowce) {
		stan->surowce -= x->Cena*ile;
		zmiana = true;
		ReleaseMutex(using_s);

		Sleep(x->Czas);

		WaitForSingleObject(using_j, INFINITE);
		stan->jazda += ile;
		zmiana = true;
		ReleaseMutex(using_j);
	}
	else {
		ReleaseMutex(using_s);
		return -1;
	}
	return		0;
}

void Miasto::setL(int l) {
	//mutex
	WaitForSingleObject(using_l, INFINITE);
	stan->l_piechota = l;
	zmiana = true;
	ReleaseMutex(using_l);
}

void Miasto::setC(int l) {
	//mutex
	WaitForSingleObject(using_c, INFINITE);
	stan->c_piechota = l;
	zmiana = true;
	ReleaseMutex(using_c);
}

void Miasto::setJ(int l) {
	//mutex
	WaitForSingleObject(using_j, INFINITE);
	stan->jazda = l;
	zmiana = true;
	ReleaseMutex(using_j);
}

void Miasto::lock() {
	WaitForSingleObject(using_c, INFINITE);
	WaitForSingleObject(using_l, INFINITE);
	WaitForSingleObject(using_j, INFINITE);
}

void Miasto::unlock() {
	ReleaseMutex(using_j);
	ReleaseMutex(using_l);
	ReleaseMutex(using_c);
}