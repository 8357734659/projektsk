#pragma once
#include<string>
#include<Windows.h>
#include<process.h>

#include"Jednostka.h"
#include"Stan.h"

class Miasto
{
public:
	Miasto(std::string);
	~Miasto();
	void wydobycie();
	void killall();
	void lock();
	void unlock();
	double sila_ataku(Jednostka*, Jednostka*, Jednostka*,int,int,int);
	double sila_obrony(Jednostka*, Jednostka*, Jednostka*,int,int,int);
	double sila_ataku(Jednostka*, Jednostka*, Jednostka*);
	double sila_obrony(Jednostka*, Jednostka*, Jednostka*);
	int treningR(int, Jednostka*);
	int treningL(int, Jednostka*);
	int treningC(int, Jednostka*);
	int treningJ(int, Jednostka*);
	//void setR(int);
	void setL(int);
	void setC(int);
	void setJ(int);

	Stan *stan;
	bool zmiana = false;

protected:
	HANDLE h;
	HANDLE using_s;
	HANDLE using_j;
	HANDLE using_l;
	HANDLE using_c;
};

