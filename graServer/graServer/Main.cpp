#include<iostream>
#include<winsock2.h>
#include<process.h>
#include<vector>
#include<cstdlib>

#include "Logika.h"

#pragma comment(lib, "Ws2_32.lib")

using namespace std;

SECURITY_ATTRIBUTES security = {
	sizeof(security), nullptr, /* bInheritHandle = */ TRUE
};
struct necessaryData {
	SOCKET* sock;
	HANDLE sending;
	Miasto* m = NULL;
	int id;
	bool flag = true;
	necessaryData(int i, SOCKET* s) {
		sending = CreateMutex(&security, false, NULL);
		sock = s;
		id = i;
	}
	necessaryData(int i) {
		sending = CreateMutex(&security, false, NULL);
		sock = new SOCKET;
		*sock = SOCKET_ERROR;
		id = i;
	}
};
struct necessaryData** nd;
struct data2 {
	struct necessaryData* d;
	char s[256];
};

const unsigned int maxNumberOfPlayers = 5;
const unsigned int toWin = 5;

SOCKET mainSocket;
HANDLE mainHandles[maxNumberOfPlayers] = { NULL };
HANDLE h = CreateMutex(&security, false, NULL);

//zamienia komunikat o polach rozdzielonych przecinkami na talic� tablic char
//typ(atak,nauka),l,c,j,r,(nazwa)
char* translate(char* t, int x[]) {
	char *tab = strtok(t, ",");
	for(int i=0 ; i<5 ; i++) {
		x[i] = atoi(tab);
		tab = strtok(NULL, ",");
	}
	return tab;
}

//szuka gracza o zadanej nazwie
Miasto* findByName(string n) {
	WaitForSingleObject(h, INFINITE);
	for (int i = 0; i < maxNumberOfPlayers; i++) {
		if (*nd[i]->sock != SOCKET_ERROR) {
			if(nd[i]->m != NULL)
				if (nd[i]->m->stan->nazwa.compare(n)==0)
					return nd[i]->m;
		}
	}
	ReleaseMutex(h);
	return NULL;
}

//og�asza istnienie nowego gracza i wysy�a mu inf. o innych
void broadcastName(char* sendbuf, int i) {
	for (int j = 0; j < maxNumberOfPlayers; j++){
		if (j != i && *nd[j]->sock != SOCKET_ERROR) {
			WaitForSingleObject(nd[j]->sending, INFINITE);
			send(*nd[j]->sock, sendbuf, strlen(sendbuf), 0);
			ReleaseMutex(nd[j]->sending);
			WaitForSingleObject(nd[i]->sending, INFINITE);
			send(*nd[i]->sock, nd[j]->m->stan->nazwa.c_str(), strlen(nd[j]->m->stan->nazwa.c_str()), 0);
			ReleaseMutex(nd[i]->sending);
		}
	}
}

//odczytuje komunikat do znaku ko�ca komunikatu lub zadany rozmiar
int receive(char delimiter, SOCKET* socket, char* recvbuf, int size) {//TODO receive przerwane?
	int bytes = 0;
	if (delimiter == NULL) {
		bytes = recv(*socket, recvbuf, size, 0);
	}
	else {
		char *a = new char;
		for (int i = 0; i < size; i++) {
			bytes += recv(*socket, a, 1, 0);
			recvbuf[i] = *a;
			if (*a == delimiter) {
				break;
			}
		}
		if (*a != delimiter) {
			return -1;
		}
	}

	return bytes;
}

//przeprowadza walk�
unsigned int __stdcall walki(void* data) {
	int w[5];
	char* x = ((data2*)data)->s;
	Miasto* m = ((data2*)data)->d->m;
	char* nazwa = translate(x, w);
	Miasto* n = findByName(string(nazwa));
	if (n != NULL) {
		int r = walka(w[1], w[2], w[3], m, n);
		if (r == -1) {
			char sendbuf[] = {"Oszust! Nie masz tylu jednostek.\n"};
			WaitForSingleObject(((data2*)data)->d->sending, INFINITE);
			send(*((data2*)data)->d->sock, sendbuf, strlen(sendbuf), 0);
			ReleaseMutex(((data2*)data)->d->sending);
		}
		if (m->stan->punkty >= toWin) {
			char sendbuf[] = { "Gratulacje, wygra�e� pi�� potyczek!\n" };
			WaitForSingleObject(((data2*)data)->d->sending, INFINITE);
			send(*((data2*)data)->d->sock, sendbuf, strlen(sendbuf), 0);
			ReleaseMutex(((data2*)data)->d->sending);
		}
	}
	else {
		char sendbuf[] = { "Oszust! Nie ma takiego przeciwnika.\n" };
		send(*((data2*)data)->d->sock, sendbuf, strlen(sendbuf), 0);
	}
	return 0;
}

//przeprowadza szkolenie
unsigned int __stdcall szkolenia(void* data) {
	int w[5];
	char* x = ((data2*)data)->s;
	Miasto* m = ((data2*)data)->d->m;
	translate(x, w);
	int r = nauka(w[4], w[1], w[2], w[3], m);
	if (r != 0) {
		char sendbuf[] = { "Oszust! Nie sta� ci� na tyle jednostek.\n" };
		WaitForSingleObject(((necessaryData*)data)->sending, INFINITE);
		send(*((data2*)data)->d->sock, sendbuf, strlen(sendbuf), 0);
		ReleaseMutex(((necessaryData*)data)->sending);
	}
	return 0;
}

//odbiera komunikaty gracza
unsigned int __stdcall receiveMess(void* data) {
	char recvbuf[256] = "";
	int w[5];
	int bytesRecv;
	vector<HANDLE> handles;
	do {//TODO receive
		bytesRecv = receive('\n', ((necessaryData*)data)->sock, recvbuf, 256);

		if (bytesRecv > 0) {
			cout << "Just received from " << ((necessaryData*)data)->id << ": " << recvbuf << "\n";
			
			data2* d = new data2;
			d->d = (necessaryData*)data;
			strcpy(d->s, recvbuf);
			if (recvbuf[0] == '1') {//atak
				handles.push_back((HANDLE)_beginthreadex(0, 0, &walki, d, 0, 0));
			}else if (recvbuf[0] == '2'){//nauka
				handles.push_back((HANDLE)_beginthreadex(0, 0, &szkolenia, d, 0, 0));
			}
			else {
				char sendbuf[] = { "Oszust! Nie ma takiej komendy!\n" };
				send(*((data2*)data)->d->sock, sendbuf, strlen(sendbuf), 0);
			}
		}
		else if (bytesRecv == 0) {
			cout << "Connection (receiving) has been closed.\n";
		}
		else {
			cout << "Error:\t" << WSAGetLastError() << "\n";
		}
		for (int i = 0; i < handles.size(); i++) {
			DWORD s = WaitForSingleObject(handles[i], 0);
			if (s == WAIT_OBJECT_0) {
				CloseHandle(handles[i]);
				handles.erase(handles.begin() + i);
			}
		}
	} while (bytesRecv > 0 && ((necessaryData*)data)->flag);

	for (int i = 0; i < handles.size(); i++){
		WaitForSingleObject(handles[i], INFINITE);
		CloseHandle(handles[i]);
	}

	return 0;
}

//WYSY�A GRACZOWI INFORMACJE O STANIE GRY
unsigned int __stdcall sendMess(void* data) {
	int bytesSent = 1;
	char* sendbuf;

	do {
		if (((necessaryData*)data)->m->zmiana)
		{
			((necessaryData*)data)->m->zmiana = false;
			sendbuf = ((necessaryData*)data)->m->stan->toString();
			WaitForSingleObject(((necessaryData*)data)->sending, INFINITE);
			bytesSent = send(*((necessaryData*)data)->sock, sendbuf, strlen(sendbuf), 0);
			ReleaseMutex(((necessaryData*)data)->sending);
			
			if (bytesSent > 0) {
				cout << "Just send to " << ((necessaryData*)data)->m->stan->nazwa << ": " << sendbuf << "\n";
				Sleep(sen);
			}
			else if (bytesSent == 0) {
				cout << "Connection (sending) has been closed.\n";
				break;
			}
			else {
				cout << "Error:\t" << WSAGetLastError() << "\n";
				break;
			}
		}
		else {
			Sleep(500);
		}
	} while (((necessaryData*)data)->flag);
	return 0;
}

//ZAMYKANIE
void exiting() {
	for (int j = 0; j < maxNumberOfPlayers; j++) {
		if(mainHandles[j]!=NULL)
			CloseHandle(mainHandles[j]);
	}

	CloseHandle(h);
	closesocket(mainSocket);
	WSACleanup();
	exit(0);
}

//UTWORZENIE W�TK�W OBS�UGUJ�CYCH GRACZA I POSPRZ�TANIE PO NICH
unsigned int __stdcall serve(void* data) {
	char recvbuf[32] = "";
	int bytesRecv;

	bytesRecv = receive('\n', ((necessaryData*)data)->sock, recvbuf, 32);
	cout << recvbuf;
	bool t = true;
	if (findByName(recvbuf) == NULL) {
		send(*((necessaryData*)data)->sock, "OK\n", strlen("OK\n"), 0);
		if (bytesRecv > 0 && t) {
			((necessaryData*)data)->m = new Miasto(string(recvbuf));
			nd[((necessaryData*)data)->id] = (necessaryData*)data;

			broadcastName(recvbuf, ((necessaryData*)data)->id);

			HANDLE handles[2];
			handles[0] = (HANDLE)_beginthreadex(0, 0, &receiveMess, data, 0, 0);
			handles[1] = (HANDLE)_beginthreadex(0, 0, &sendMess, data, 0, 0);
			WaitForMultipleObjects(2, handles, false, INFINITE);
			nd[((necessaryData*)data)->id]->flag = false;
			WaitForMultipleObjects(2, handles, true, INFINITE);
			for (int i = 0; i < 2; i++) CloseHandle(handles[i]);
			delete ((necessaryData*)data)->m;
			((necessaryData*)data)->m = NULL;
		}
	}
	closesocket(*((necessaryData*)data)->sock);
	cout << "Client " << ((necessaryData*)data)->id << " with socket (" << *(nd[((necessaryData*)data)->id]->sock) << ") disconnected" << endl;
	*((necessaryData*)data)->sock = SOCKET_ERROR;
	cout << "Client " << ((necessaryData*)data)->id << " with socket (" << *(nd[((necessaryData*)data)->id]->sock) << ") disconnected" << endl;
	return 0;
}

//INICJALIZACJA SOCKET�W
SOCKET initializeSockets(const char* ip, int port) {
	WSADATA wsaData;
	int result = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (result != NO_ERROR)
		cout << "Initialization error (" << result << ").\n";
	SOCKET mainSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (mainSocket == INVALID_SOCKET) {
		cout << "Error creating socket: " << WSAGetLastError();
		WSACleanup();
		return 1;
	}

	sockaddr_in service;
	memset(&service, 0, sizeof(service));
	service.sin_family = AF_INET;
	service.sin_addr.s_addr = inet_addr(ip);
	service.sin_port = htons(port);

	int r = bind(mainSocket, (SOCKADDR *)&service, sizeof(service));
	if (r == SOCKET_ERROR) {
		cout << "Binding failed.\n";
		closesocket(mainSocket);
		return 1;
	}

	r = listen(mainSocket,SOMAXCONN);
	if (r == SOCKET_ERROR) {
		cout << "Listening failed.\n";
		closesocket(mainSocket);
		return 1;
	}

	return mainSocket;
}

int main() {
	atexit(exiting);
	at_quick_exit(exiting);

	string ip;
	int port;
	cout << "Czy chcesz korzystac z adresu 127.0.0.1 i portu 27015 [t\\n]?\n";
	char yn = (char)getchar();
	while (getchar() != '\n');
	if (yn == 't') {
		ip = "127.0.0.1";
		port = 27015;
	}
	else {
		if (yn != 'n')cout << "Nie rozumiem co napisales, przyjmuje ze bylo to: 'nie'\n";
		cout << "IP:";
		cin >> ip;
		cout << "Port:";
		cin >> port;
	}
	
	//INICJALIZACJA ZMIENNYCH
	mainSocket = initializeSockets(ip.c_str(), port);

	if (mainSocket == 1) exit(-1);

	SOCKET		** acceptSocket = new SOCKET*[maxNumberOfPlayers];
	
	int i = 0;

	for (int i = 0; i < maxNumberOfPlayers; i++) {
		acceptSocket[i] = new SOCKET;
		*(acceptSocket[i]) = SOCKET_ERROR;
	}
	nd = new necessaryData*[maxNumberOfPlayers];
	for (int j = 0; j < maxNumberOfPlayers; j++) nd[j] = new necessaryData(j);

	//��CZENIE SI� Z KLIENTAMI
	while (1) { 
		if (*(acceptSocket[i]) != SOCKET_ERROR) { 
			//cout << "Waiting for a client " << i << " to disconnect...\n";
			Sleep(1000); 
			i = (i + 1) % maxNumberOfPlayers;
			continue;
		}
		cout << "Waiting for a client " << i << " to connect...\n";
		while (*(acceptSocket[i]) == SOCKET_ERROR) {
			*(acceptSocket[i]) = accept(mainSocket, NULL, NULL);
		}
		cout << "Client connected.\n";

		if (mainHandles[i] != NULL) {
			CloseHandle(mainHandles[i]);
			mainHandles[i] = NULL;
		}
		necessaryData* t = new necessaryData(i, acceptSocket[i]);
		mainHandles[i] = (HANDLE)_beginthreadex(0, 0, &serve, t, 0, 0);
		
		i=(i+1)%maxNumberOfPlayers;
	}
}
