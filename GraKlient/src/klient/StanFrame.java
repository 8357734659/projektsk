package klient;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class StanFrame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1230660998427076359L;
	private JPanel panel_1;
	private JLabel[] label = new JLabel[6];

	public StanFrame(String x) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		panel_1 = new JPanel();
		setSize(150,170);
		panel_1.setBorder(new EmptyBorder(5, 5, 5, 5));
		panel_1.setLayout(null);
		setContentPane(panel_1);
		
		JLabel lblPunkty = new JLabel("Punkty:");
		lblPunkty.setBounds(10, 10, 89, 23);
		panel_1.add(lblPunkty);
		
		JLabel lblSurowce = new JLabel("Surowce:");
		lblSurowce.setBounds(10, 30, 89, 23);
		panel_1.add(lblSurowce);
		
		JLabel lblRobotnicy = new JLabel("Robotnicy:");
		lblRobotnicy.setBounds(10, 50, 89, 23);
		panel_1.add(lblRobotnicy);
		
		JLabel lblJazda = new JLabel("Lekka piechota:");
		lblJazda.setBounds(10, 70, 80, 23);
		panel_1.add(lblJazda);
		
		JLabel lblLekkaPiechota = new JLabel("Ci\u0119\u017Cka piechota:");
		lblLekkaPiechota.setBounds(10, 90, 89, 23);
		panel_1.add(lblLekkaPiechota);
		
		JLabel lblCikaPiechota = new JLabel("Jazda:");
		lblCikaPiechota.setBounds(10, 110, 89, 23);
		panel_1.add(lblCikaPiechota);
		
		String[] y = x.split(" ");
		if(y.length!=6)
			JOptionPane.showMessageDialog(new JFrame(),"Nie pe�na informacja.");

		for(int i=0 ; i<label.length ; i++){
			label[i] = new JLabel(y[i]);
			label[i].setBounds(100, 20*i+10, 46, 14);
			panel_1.add(label[i]);
		}
	}

}
