package klient;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class AtakFrame extends JDialog {

	private static final long serialVersionUID = -666482667914774491L;
	private JPanel contentPane;
	JTextField txtJazda;
	JTextField txtLekkaPiechota;
	JTextField txtCikaPiechota;
	JComboBox<String> cbWrog;
	
	String w=null;

	public AtakFrame(String[] s) {
		setModal(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
		    @Override
		    public void windowClosing(WindowEvent e) {
		        w = "";
		    }
		});
		setBounds(100, 100, 194, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 90, 120);
		contentPane.add(panel);
		panel.setLayout(null);
		
		JLabel lblRobotnicy = new JLabel("Przeciwnik:");
		lblRobotnicy.setBounds(0,0,90, 25);
		panel.add(lblRobotnicy);
		
		JLabel lblJazda = new JLabel("Jazda:");
		lblJazda.setBounds(0,30,90, 25);
		panel.add(lblJazda);
		
		JLabel lblLekkaPiechota = new JLabel("Lekka piechota:");
		lblLekkaPiechota.setBounds(0,60,90, 25);
		panel.add(lblLekkaPiechota);
		
		JLabel lblCikaPiechota = new JLabel("Ci\u0119\u017Cka piechota:");
		lblCikaPiechota.setBounds(0,90,90, 25);
		panel.add(lblCikaPiechota);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(92, 0, 90, 120);
		contentPane.add(panel_1);
		panel_1.setLayout(null);
		
		cbWrog = new JComboBox<String>(s);
		cbWrog.setBounds(0,0,90, 25);
		panel_1.add(cbWrog);
		
		txtJazda = new JTextField("0");
		txtJazda.setBounds(0,30,90, 25);
		panel_1.add(txtJazda);
		
		txtLekkaPiechota = new JTextField("0");
		txtLekkaPiechota.setBounds(0,60,90, 25);
		panel_1.add(txtLekkaPiechota);
		
		txtCikaPiechota = new JTextField("0");
		txtCikaPiechota.setBounds(0,90,90, 25);
		panel_1.add(txtCikaPiechota);
		
		JButton btnOK = new JButton("OK");
		this.getRootPane().setDefaultButton(btnOK);
		btnOK.setBounds(45,120,90,25);
		btnOK.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							setVisible(false);
							dispose();
						} catch (Exception e1) {
							StringWriter sw = new StringWriter();
			            	e1.printStackTrace(new PrintWriter(sw));
			            	String s="";
			            	for(String ss:sw.toString().split("\n")){
			            		if(!ss.substring(0,4).equals("\tat "))
			            			s+=ss+"\n";
			            	}
							JOptionPane.showMessageDialog(new JFrame(),s);
						}
					}
				});
			}
		});		
		contentPane.add(btnOK);
	}
	
	public String showDialog(){
		setVisible(true);
		if(!txtLekkaPiechota.getText().matches("\\b\\d+\\b")) return null;
		if(!txtCikaPiechota.getText().matches("\\b\\d+\\b")) return null;
		if(!txtJazda.getText().matches("\\b\\d+\\b")) return null;
		if(cbWrog.getSelectedItem()==null) return null;
		if(txtLekkaPiechota.getText().matches("\\b0\\b") && 
				txtCikaPiechota.getText().matches("\\b0\\b") && 
				txtJazda.getText().matches("\\b0\\b")) return null;
		w = "1,"+
				txtLekkaPiechota.getText()+","+
				txtCikaPiechota.getText()+","+
				txtJazda.getText()+","+
				"0"+","+
				cbWrog.getSelectedItem();
		return w;			
	}

}
