package klient;

import java.awt.EventQueue;
import java.io.PrintWriter;
import java.io.StringWriter;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class Main {
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TheFrame frame = new TheFrame();
					frame.setVisible(true);
				} catch (Exception e1) {
					StringWriter sw = new StringWriter();
	            	e1.printStackTrace(new PrintWriter(sw));
	            	String s="";
	            	for(String ss:sw.toString().split("\n")){
	            		if(!ss.substring(0,4).equals("\tat "))
	            			s+=ss+"\n";
	            	}
					JOptionPane.showMessageDialog(new JFrame(),s);
				}
			}
		});
	}
}
