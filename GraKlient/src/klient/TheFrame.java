package klient;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.awt.event.ActionEvent;

public class TheFrame extends JFrame {
	private static final long serialVersionUID = 1553229245086546672L;
	private JPanel contentPane;
	private JPanel panel;
	private JPanel panel_1;
	private JTextField textField;
	private JTextField ipField;
	private JTextField portField;
	
	private Thread fS;
	private Thread tS;
	private Thread k;
	
	private String nazwa;
	private String ip = "127.0.0.1";
	private int port = 27015;
	
	volatile String serverMessage="";
	volatile Set<String> komunikaty = new HashSet<String>();
	volatile ArrayList<String> clientMessage = new ArrayList<String>();
	volatile Set<String> nazwy = new HashSet<String>(Arrays.asList(new String[]{""}));
	volatile Socket socket;
	volatile boolean flag = true;
	
	BufferedReader reader;
	
	public TheFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		panel = new JPanel();
		panel.setPreferredSize(new Dimension(300, 90));
		contentPane.add(panel);
		panel.setLayout(null);
		
		textField = new JTextField();
		ipField = new JTextField("127.0.0.1");
		portField = new JTextField("27015");
		textField.setBounds(61, 15, 130, 20);
		ipField.setBounds(61, 40, 130, 20);
		portField.setBounds(61, 65, 130, 20);
		panel.add(textField);
		panel.add(ipField);
		panel.add(portField);
		
		JButton btnOk = new JButton("OK");
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!(textField.getText().equals("") || 
						textField.getText().contains(" ") || 
						textField.getText().contains("\n") ||
						textField.getText().length()>31)){
					nazwa = textField.getText();
					ip=ipField.getText();
					try {
						port=Integer.parseInt(portField.getText());
						socket = new Socket(ip,port);
						System.out.println(socket);
						socket.setSoTimeout(60*1000);
						PrintWriter writer;
						writer = new PrintWriter(socket.getOutputStream(), true);
						writer.println(nazwa);
						reader = new BufferedReader(
								new InputStreamReader(
										socket.getInputStream()));
						String x = null;
						try {
							x = reader.readLine();
						} catch (SocketTimeoutException e1) {
			                // timeout exception.
							JOptionPane.showMessageDialog(new JFrame(),"Timeout reached!");
			            }catch (SocketException e1) {
			                // timeout exception.
			            	System.out.println(e1.getMessage());
							JOptionPane.showMessageDialog(new JFrame(),"Nie ma serwera!");
							try {
								logout(true,true,true);
							} catch (Exception e2) {
								StringWriter sw = new StringWriter();
				            	e2.printStackTrace(new PrintWriter(sw));
				            	String s="";
				            	for(String ss:sw.toString().split("\n")){
				            		if(!ss.substring(0,4).equals("\tat "))
				            			s+=ss+"\n";
				            	}
								JOptionPane.showMessageDialog(new JFrame(),s);
							}
			            }catch (Exception e1) {
							JOptionPane.showMessageDialog(new JFrame(),"Ta nazwa jest ju� zaj�ta!");
						}

						if (x.equals("OK")) {
							setTitle(nazwa);
							panel.setVisible(false);
							
							fS = fromServer();
							tS = toServer();
							k = messeges();
							fS.setName("fromServer");
							tS.setName("toServer");
							k.setName("kom");
							fS.start();
							tS.start();
							k.start();
							initB();
						}
					} catch (SocketTimeoutException e1) {
		                // timeout exception.
						JOptionPane.showMessageDialog(new JFrame(),"Timeout reached!");
		            }catch (Exception e1) {
		            	StringWriter sw = new StringWriter();
		            	e1.printStackTrace(new PrintWriter(sw));
		            	String s="";
		            	for(String ss:sw.toString().split("\n")){
		            		if(!ss.substring(0,4).equals("\tat "))
		            			s+=ss+"\n";
		            	}
						JOptionPane.showMessageDialog(new JFrame(),s);
					}
				}
				else{
					JOptionPane.showMessageDialog(new JFrame(),"Taka nazwa nie przejdzie");
				}
			}
		});
		btnOk.setBounds(201, 11, 89, 23);
		this.getRootPane().setDefaultButton(btnOk);
		panel.add(btnOk);
		
		JLabel lblNazwa = new JLabel("Nazwa:");
		JLabel lblIP = new JLabel("IP:");
		JLabel lblPort = new JLabel("Port:");
		lblNazwa.setBounds(5, 15, 46, 14);
		lblIP.setBounds(5, 40, 46, 14);
		lblPort.setBounds(5, 65, 46, 14);
		panel.add(lblNazwa);
		panel.add(lblIP);
		panel.add(lblPort);
		
		panel.setVisible(true);
		pack();		
	}
	
	protected void initB() throws UnknownHostException, IOException, InterruptedException{
		panel_1 = new JPanel();
		panel_1.setLayout(new FlowLayout());
		contentPane.add(panel_1);
		
		JButton btnAtak = new JButton("Atak");
		btnAtak.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							AtakFrame frame = new AtakFrame(nazwy.toArray(new String[nazwy.size()]));
							String x = frame.showDialog();
							if(x!=null && !x.equals("")){
								clientMessage.add(x);
							}
							else if(!x.equals(""))
								JOptionPane.showMessageDialog(new JFrame(),"Jedno z p�l jest puste!");
						} catch (Exception e1) {
							StringWriter sw = new StringWriter();
			            	e1.printStackTrace(new PrintWriter(sw));
			            	String s="";
			            	for(String ss:sw.toString().split("\n")){
			            		if(!ss.substring(0,4).equals("\tat "))
			            			s+=ss+"\n";
			            	}
							JOptionPane.showMessageDialog(new JFrame(),s);
						}
					}
				});
			}
		});
		panel_1.add(btnAtak);
		
		JButton btnSzkolenie = new JButton("Szkolenie");
		btnSzkolenie.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							SzkolenieFrame frame = new SzkolenieFrame();
							String x = frame.showDialog();
							if(x!=null && !x.equals("")){
								clientMessage.add(x);
							}else if(!x.equals(""))
								JOptionPane.showMessageDialog(new JFrame(),"Jedno z p�l jest puste!");
						} catch (Exception e1) {
							StringWriter sw = new StringWriter();
			            	e1.printStackTrace(new PrintWriter(sw));
			            	String s="";
			            	for(String ss:sw.toString().split("\n")){
			            		if(!ss.substring(0,4).equals("\tat "))
			            			s+=ss+"\n";
			            	}
							JOptionPane.showMessageDialog(new JFrame(),s);
						}
					}
				});
			}
		});		
		panel_1.add(btnSzkolenie);

		JButton btnStan = new JButton("Stan");
		btnStan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							StanFrame frame = new StanFrame(serverMessage);
							frame.setVisible(true);
						} catch (Exception e1) {
							StringWriter sw = new StringWriter();
			            	e1.printStackTrace(new PrintWriter(sw));
			            	String s="";
			            	for(String ss:sw.toString().split("\n")){
			            		if(!ss.substring(0,4).equals("\tat "))
			            			s+=ss+"\n";
			            	}
							JOptionPane.showMessageDialog(new JFrame(),s);
						}
					}
				});
			}
		});
		panel_1.add(btnStan);
		
		JButton btnKoniec = new JButton("Koniec");
		this.getRootPane().setDefaultButton(btnKoniec);
		btnKoniec.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							logout(true,true,true);
						} catch (Exception e1) {
							StringWriter sw = new StringWriter();
			            	e1.printStackTrace(new PrintWriter(sw));
			            	String s="";
			            	for(String ss:sw.toString().split("\n")){
			            		if(!ss.substring(0,4).equals("\tat "))
			            			s+=ss+"\n";
			            	}
							JOptionPane.showMessageDialog(new JFrame(),s);
						}
					}
				});
			}
		});
		panel_1.add(btnKoniec);
		
		panel_1.setVisible(true);
		panel.setVisible(false);
		pack();
	}
	
	protected void logout(boolean a, boolean b, boolean c) throws InterruptedException, IOException{
		flag = false;
		if(a)fS.join();
		if(b)tS.join();
		if(c)k.join();
		socket.close();
		
		serverMessage="";
		komunikaty.clear();
		clientMessage.clear();
		nazwy.clear();
		flag = true;
		
		panel_1.setVisible(false);
		panel.setVisible(true);
		pack();
	}

	protected Thread messeges(){
		Thread thread = new Thread(()->{
			try{
				while(flag){
					if(!komunikaty.isEmpty()){
						for(String x : komunikaty){
							JOptionPane.showMessageDialog(new JFrame(), x);
							String y = x.split(" ")[0];
							if(y.equals("Gratulacje,")){
								flag=false;
								break;
							}
						}
						komunikaty.clear();
					}else{
						Thread.sleep(500);
					}
				}
			}catch(Exception e1){
				StringWriter sw = new StringWriter();
            	e1.printStackTrace(new PrintWriter(sw));
            	String s="";
            	for(String ss:sw.toString().split("\n")){
            		if(!ss.substring(0,4).equals("\tat "))
            			s+=ss+"\n";
            	}
				JOptionPane.showMessageDialog(new JFrame(),s);
			}
		});
		return thread;
	}
	
	protected Thread fromServer(){
		Thread thread = new Thread(()->{
			try{			
				String x;
				
				while(flag){
					x = reader.readLine();
					
					if(x.split(" ").length==6)
						serverMessage=x;
					else{
						System.out.println(x+"|");
						if (x.split(" ").length==1)
							nazwy.add(x);
						else
							komunikaty.add(x);
					}
				}
			}catch (SocketException e1) {
                // timeout exception.
            	System.out.println(e1.getMessage());
				JOptionPane.showMessageDialog(new JFrame(),"Nie ma serwera!");
				try {
					logout(false,true,true);
				} catch (Exception e2) {
					StringWriter sw = new StringWriter();
	            	e2.printStackTrace(new PrintWriter(sw));
	            	String s="";
	            	for(String ss:sw.toString().split("\n")){
	            		if(!ss.substring(0,4).equals("\tat "))
	            			s+=ss+"\n";
	            	}
					JOptionPane.showMessageDialog(new JFrame(),s);
				}
            }catch(Exception e1){
				StringWriter sw = new StringWriter();
            	e1.printStackTrace(new PrintWriter(sw));
            	String s="";
            	for(String ss:sw.toString().split("\n")){
            		if(!ss.substring(0,4).equals("\tat "))
            			s+=ss+"\n";
            	}
				JOptionPane.showMessageDialog(new JFrame(),s);
			}
		});
		return thread;
	}

	protected Thread toServer(){
		Thread thread = new Thread(()->{
			try{
				PrintWriter writer;
				writer = new PrintWriter(socket.getOutputStream(), true);
				
				String x;
				while(flag){
					if(!clientMessage.isEmpty()){
						x = clientMessage.get(0);
						clientMessage.remove(0);
						writer.println(x);
					}else{
						Thread.sleep(500);
					}
				}
			}catch (SocketException e1) {
                // timeout exception.
            	System.out.println(e1.getMessage());
				JOptionPane.showMessageDialog(new JFrame(),"Nie ma serwera!");
				try {
					logout(true,false,true);
				} catch (Exception e2) {
					StringWriter sw = new StringWriter();
	            	e2.printStackTrace(new PrintWriter(sw));
	            	String s="";
	            	for(String ss:sw.toString().split("\n")){
	            		if(!ss.substring(0,4).equals("\tat "))
	            			s+=ss+"\n";
	            	}
					JOptionPane.showMessageDialog(new JFrame(),s);
				}
            }catch(Exception e1){
				StringWriter sw = new StringWriter();
            	e1.printStackTrace(new PrintWriter(sw));
            	String s="";
            	for(String ss:sw.toString().split("\n")){
            		if(!ss.substring(0,4).equals("\tat "))
            			s+=ss+"\n";
            	}
				JOptionPane.showMessageDialog(new JFrame(),s);
			}
		});
		return thread;
	}
}

