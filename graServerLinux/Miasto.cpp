#include "Miasto.h"

void* tmp(void* data) {
	((Miasto*) data)->wydobycie();
	pthread_exit(NULL);
}

void Miasto::wydobycie()
{
	int ile = 50;
	int co_ile = 1;
	int wplyw = 5;
	while (stan->t) {
		pthread_mutex_lock(&using_s);
		stan->surowce += (ile + wplyw * stan->robotnicy);
		zmiana = true;
		pthread_mutex_unlock(&using_s);
		sleep(co_ile);
	}
}

void Miasto::killall()
{
	this->lock();
	stan->c_piechota = 0;
	stan->l_piechota = 0;
	stan->jazda = 0;
	zmiana = true;
	this->unlock();
}

Miasto::Miasto(std::string nazwa)
{
	this->stan = new Stan(nazwa);
	pthread_create(&h, NULL, tmp, (void *)this);
    	
	this->using_s = PTHREAD_MUTEX_INITIALIZER;
	this->using_j = PTHREAD_MUTEX_INITIALIZER;
	this->using_l = PTHREAD_MUTEX_INITIALIZER;
	this->using_c = PTHREAD_MUTEX_INITIALIZER;

	zmiana = false;
}

Miasto::~Miasto()
{
	this->stan->t = false;
	pthread_join(this->h,NULL);
	delete stan;
}

double Miasto::sila_ataku(Jednostka *j, Jednostka *l, Jednostka *c,int lj, int ll , int lc)
{
	return lj*j->Atak + ll*l->Atak + lc*c->Atak;
}

double Miasto::sila_ataku(Jednostka *j, Jednostka *l, Jednostka *c)
{
	return stan->jazda*j->Atak + stan->l_piechota*l->Atak + stan->c_piechota*c->Atak;
}

double Miasto::sila_obrony(Jednostka *j, Jednostka *l, Jednostka *c, int lj, int ll, int lc)
{
	return lj*j->Obrona + ll*l->Obrona + lc*c->Obrona;
}

double Miasto::sila_obrony(Jednostka *j, Jednostka *l, Jednostka *c)
{
	return stan->jazda*j->Obrona + stan->l_piechota*l->Obrona + stan->c_piechota*c->Obrona;
}

int Miasto::treningR(int ile, Jednostka *x)
{//mutex
	pthread_mutex_lock(&using_s);
	if (ile >= 0 && ile*x->Cena<= stan->surowce) {
		stan->surowce -= x->Cena*ile;
		zmiana = true;
		pthread_mutex_unlock(&using_s);
		sleep(x->Czas);
		stan->robotnicy += ile;
		zmiana = true;
	}
	else {
		pthread_mutex_unlock(&using_s);
		return -1;
	}
	return		0;
}

int Miasto::treningL(int ile, Jednostka *x)
{
	pthread_mutex_lock(&using_s);
	if (ile >= 0 && ile*x->Cena <= stan->surowce) {
		stan->surowce -= x->Cena*ile;
		zmiana = true;
		pthread_mutex_unlock(&using_s);
		
		sleep(x->Czas);

		pthread_mutex_lock(&using_l);
		stan->l_piechota += ile;
		zmiana = true;
		pthread_mutex_unlock(&using_l);
	}
	else {
		pthread_mutex_unlock(&using_s);
		return -1;
	}
	return		0;
}

int Miasto::treningC(int ile, Jednostka *x)
{//mutex
	pthread_mutex_lock(&using_s);
	if (ile >= 0 && ile*x->Cena <= stan->surowce) {
		stan->surowce -= x->Cena*ile;
		zmiana = true;
		pthread_mutex_unlock(&using_s);

		sleep(x->Czas);

		pthread_mutex_lock(&using_c);
		stan->c_piechota += ile;
		zmiana = true;
		pthread_mutex_unlock(&using_c);
	}
	else {
		pthread_mutex_unlock(&using_s);
		return -1;
	}
	return		0;
}

int Miasto::treningJ(int ile, Jednostka *x)
{//mutex
	pthread_mutex_lock(&using_s);
	if (ile >= 0 && ile*x->Cena <= stan->surowce) {
		stan->surowce -= x->Cena*ile;
		zmiana = true;
		pthread_mutex_unlock(&using_s);

		sleep(x->Czas);

		pthread_mutex_lock(&using_j);
		stan->jazda += ile;
		zmiana = true;
		pthread_mutex_unlock(&using_j);
	}
	else {
		pthread_mutex_unlock(&using_s);
		return -1;
	}
	return		0;
}

void Miasto::setL(int l) {
	//mutex
	pthread_mutex_lock(&using_l);
	stan->l_piechota = l;
	zmiana = true;
	pthread_mutex_unlock(&using_l);
}

void Miasto::setC(int l) {
	//mutex
	pthread_mutex_lock(&using_c);
	stan->c_piechota = l;
	zmiana = true;
	pthread_mutex_unlock(&using_c);
}

void Miasto::setJ(int l) {
	//mutex
	pthread_mutex_lock(&using_j);
	stan->jazda = l;
	zmiana = true;
	pthread_mutex_unlock(&using_j);
}

void Miasto::lock() {
	pthread_mutex_lock(&using_c);
	pthread_mutex_lock(&using_l);
	pthread_mutex_lock(&using_j);
}

void Miasto::unlock() {
	pthread_mutex_unlock(&using_j);
	pthread_mutex_unlock(&using_l);
	pthread_mutex_unlock(&using_c);
}
