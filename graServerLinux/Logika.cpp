#include"Logika.h"
#include<iostream>

Jednostka *l_Piechota = new Jednostka(100,1,1.2,2);
Jednostka *c_Piechota = new Jednostka(250,1.5 ,3 ,3 );
Jednostka *jezdziec = new Jednostka(550,3.5 ,1.2 ,5 );
Jednostka *robotnik = new Jednostka(150,0 ,0 ,2 );

int walka(int l, int c, int j, Miasto* atak, Miasto *obron)
{
	if (atak!=NULL && obron!=NULL)
	{
		atak->lock();
		if (atak->stan->l_piechota - l >= 0 && atak->stan->c_piechota - c >= 0 && atak->stan->jazda - j >= 0) {
			atak->unlock();
			atak->setL(atak->stan->l_piechota - l);
			atak->setC(atak->stan->c_piechota - c);
			atak->setJ(atak->stan->jazda - j);

			obron->lock();
			double so1 = obron->sila_obrony(jezdziec, l_Piechota, c_Piechota);
			double sa2 = obron->sila_ataku(jezdziec, l_Piechota, c_Piechota);
			obron->unlock();

			double sa1 = atak->sila_ataku(jezdziec, l_Piechota, c_Piechota, j, l, c);
			double so2 = atak->sila_obrony(jezdziec, l_Piechota, c_Piechota, j, l, c);
			if (sa1 - so1 > 0) {
				obron->killall();
			}
			else {
				obron->setC(obron->stan->c_piechota - floor(obron->stan->c_piechota*sa1 / so1));
				obron->setL(obron->stan->l_piechota - floor(obron->stan->l_piechota*sa1 / so1));
				obron->setJ(obron->stan->jazda - floor(obron->stan->jazda*sa1 / so1));
			}

			if (sa2 - so2 > 0) {
				l = 0;
				j = 0;
				c = 0;
			}
			else {
				l -= floor(l*sa2 / so2);
				j -= floor(j*sa2 / so2);
				c -= floor(c*sa2 / so2);
			}
			cout<<sa2<<" "<<so2<<endl;

			sleep(1);
			if (sa1 - so1 > 0) 
				atak->stan->punkty++;
			atak->setL(atak->stan->l_piechota + l);
			atak->setC(atak->stan->c_piechota + c);
			atak->setJ(atak->stan->jazda + j);
			cout<<atak->stan->toString()<<endl;
		}
		else {
			atak->unlock();
			return -1;
		}
		return 0;
	}
	return -2;
}

int nauka(int r, int l, int c, int j, Miasto* m) {
	int res = m->treningR(r, robotnik);
	res += 2*m->treningL(l, l_Piechota);
	res += 4 * m->treningC(c, c_Piechota);
	res += 8 * m->treningJ(j, jezdziec);
	return res;
}
