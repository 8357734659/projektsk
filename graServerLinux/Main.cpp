#include<iostream>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <netdb.h>
#include <signal.h>

#include<vector>
#include<cstdlib>
#include<stdio.h>
#include<string.h>
#include <pthread.h>
#include<errno.h>

#include "Logika.h"

using namespace std;

struct necessaryData {
	int* sock;
	pthread_mutex_t sending;
	Miasto* m = NULL;
	int id;
	bool flag;
	necessaryData(int i, int* s) {
		flag = true;
		sending = PTHREAD_MUTEX_INITIALIZER;
		sock = s;
		id = i;
	}
	necessaryData(int i) {
		flag = true;
		sending = PTHREAD_MUTEX_INITIALIZER;
		sock = new int;
		*sock = -1;
		id = i;
	}
};
struct necessaryData** nd;
struct data2 {
	struct necessaryData* d;
	char s[256];
};

const int maxNumberOfPlayers = 5;
const int toWin = 5;

int mainSocket;
pthread_t mainHandles[maxNumberOfPlayers] = { 0 };
pthread_mutex_t h = PTHREAD_MUTEX_INITIALIZER;

//zamienia komunikat o polach rozdzielonych przecinkami na talic� tablic char
//typ(atak,nauka),l,c,j,r,(nazwa)
char* translate(char* t, int x[]) {
	char *tab = strtok(t, ",");
	for(int i=0 ; i<5 ; i++) {
		x[i] = atoi(tab);
		tab = strtok(NULL, ",");
	}
	return tab;
}

//szuka gracza o zadanej nazwie
Miasto* findByName(string n) {
	pthread_mutex_lock(&h);
	for (int i = 0; i < maxNumberOfPlayers; i++) {
		if (*nd[i]->sock != -1) {
			if(nd[i]->m != NULL)
				if (nd[i]->m->stan->nazwa.compare(n)==0){
					pthread_mutex_unlock(&h);						
					return nd[i]->m;
				}
		}
	}
	pthread_mutex_unlock(&h);
	return NULL;
}

//og�asza istnienie nowego gracza i wysy�a mu inf. o innych
void broadcastName(char* sendbuf, int i, necessaryData* d) {
	pthread_mutex_lock(&h);
	for (int j = 0; j < maxNumberOfPlayers; j++){
		if (j != i && *nd[j]->sock != -1) {
			cout<<*d->sock<<endl;

			pthread_mutex_lock(&nd[i]->sending);

			if(send(*nd[i]->sock, nd[j]->m->stan->nazwa.c_str(), strlen(nd[j]->m->stan->nazwa.c_str()), 0)<0){
				cout<<errno;//gracza
				pthread_mutex_unlock(&nd[j]->sending);
				break;
			}
			pthread_mutex_unlock(&nd[i]->sending);

			pthread_mutex_lock(&nd[j]->sending);
			if(send(*nd[j]->sock, sendbuf, strlen(sendbuf), 0)<0)
				cout<<errno; //o graczu
			pthread_mutex_unlock(&nd[j]->sending);
		}
	}
	pthread_mutex_unlock(&h);
}

//odczytuje komunikat do znaku ko�ca komunikatu lub zadany rozmiar
int receive(char delimiter, int* socket, char* recvbuf, int size) {//TODO receive przerwane?
	int bytes = 0;
	if (delimiter=='\0') {
		bytes = recv(*socket, recvbuf, size, 0);
	}
	else {
		char *a = new char;
		for (int i = 0; i < size; i++) {
			bytes += recv(*socket, a, 1, 0);
			recvbuf[i] = *a;
			if (*a == delimiter) {
				break;
			}
		}
		if (*a != delimiter) {
			return -1;
		}
	}

	return bytes;
}

//przeprowadza walk�
void* walki(void* data) {
	int w[5];
	char* x = ((data2*)data)->s;
	Miasto* m = ((data2*)data)->d->m;
	char* nazwa = translate(x, w);
	Miasto* n = findByName(string(nazwa));
	if (n != NULL) {
		int r = walka(w[1], w[2], w[3], m, n);
		if (r == -1) {
			char sendbuf[] = {"Oszust! Nie masz tylu jednostek.\n"};
			pthread_mutex_lock(&((data2*)data)->d->sending);
			send(*((data2*)data)->d->sock, sendbuf, strlen(sendbuf), 0);
			pthread_mutex_unlock(&((data2*)data)->d->sending);
		}
		if (m->stan->punkty >= toWin) {
			char sendbuf[] = { "Gratulacje, wygra�e� pi�� potyczek!\n" };
			pthread_mutex_unlock(&((data2*)data)->d->sending);
			send(*((data2*)data)->d->sock, sendbuf, strlen(sendbuf), 0);
			pthread_mutex_unlock(&((data2*)data)->d->sending);
		}
	}
	else {
		char sendbuf[] = { "Oszust! Nie ma takiego przeciwnika.\n" };
		pthread_mutex_lock(&((data2*)data)->d->sending);
		send(*((data2*)data)->d->sock, sendbuf, strlen(sendbuf), 0);
		pthread_mutex_unlock(&((data2*)data)->d->sending);
	}
	pthread_exit(NULL);
}

//przeprowadza szkolenie
void* szkolenia(void* data) {
	int w[5];
	char* x = ((data2*)data)->s;
	Miasto* m = ((data2*)data)->d->m;
	translate(x, w);
	int r = nauka(w[4], w[1], w[2], w[3], m);
	if (r != 0) {
		char sendbuf[] = { "Oszust! Nie sta� ci� na tyle jednostek.\n" };
		pthread_mutex_lock(&((data2*)data)->d->sending);
		send(*((data2*)data)->d->sock, sendbuf, strlen(sendbuf), 0);
		pthread_mutex_unlock(&((data2*)data)->d->sending);
	}
	pthread_exit(NULL);
}

//odbiera komunikaty gracza
void* receiveMess(void* data) {
	char recvbuf[256] = "";
	int bytesRecv;
	vector<pthread_t> handles;
	do {
		cout<<"Waiting\n";
		bytesRecv = receive('\n', ((necessaryData*)data)->sock, recvbuf, 256);

		if (bytesRecv > 0) {
			cout << "Just received from " << ((necessaryData*)data)->id << ": " << recvbuf << "\n";
			
			data2* d = new data2;
			d->d = (necessaryData*)data;
			strcpy(d->s, recvbuf);
			
			if (recvbuf[0] == '1') {//atak
				pthread_t w;
				pthread_create(&w, NULL, walki, (void *)d);
				handles.push_back(w);
			}else if (recvbuf[0] == '2'){//nauka
				pthread_t w;
				pthread_create(&w, NULL, szkolenia, (void *)d);
				handles.push_back(w);
			}
			else {
				char sendbuf[] = { "Oszust! Nie ma takiej komendy!\n" };
				send(*((data2*)data)->d->sock, sendbuf, strlen(sendbuf), 0);
			}
		}
		else if (bytesRecv == 0) {
			cout << "Connection (receiving) has been closed.\n";
		}
		else {
			cout << "Error:\t" << errno << " (r)\n";
		}
		for (unsigned int i = 0; i < handles.size(); i++) {
			int s;
			s=pthread_tryjoin_np(handles[i], NULL);
			if (s == 0) {
				handles.erase(handles.begin() + i);
			}
		}
	} while (bytesRecv > 0 && ((necessaryData*)data)->flag);

	for (unsigned int i = 0; i < handles.size(); i++){
		pthread_join(handles[i],NULL);
	}
	cout<<"Stopped receiving.\n";
	pthread_exit(NULL);
}

//WYSY�A GRACZOWI INFORMACJE O STANIE GRY
void* sendMess(void* data) {
	int bytesSent = 1;
	char* sendbuf;

	do {
		if (((necessaryData*)data)->m->zmiana)
		{
			((necessaryData*)data)->m->zmiana = false;
			sendbuf = ((necessaryData*)data)->m->stan->toString();
			pthread_mutex_lock(&((necessaryData*)data)->sending);
			bytesSent = send(*((necessaryData*)data)->sock, sendbuf, strlen(sendbuf), 0);
			pthread_mutex_unlock(&((necessaryData*)data)->sending);
			
			if (bytesSent > 0) {
				//cout << "Just send to " << ((necessaryData*)data)->m->stan->nazwa << ": " << sendbuf << "\n";
				sleep(sen*5);
			}
			else if (bytesSent == 0) {
				cout << "Connection (sending) has been closed.\n";
				break;
			}
			else {
				cout << "Error:\t" << errno << " (s)\n";
				break;
			}
		}
		else {
			sleep(1);
		}
	} while (((necessaryData*)data)->flag);
	cout<<"Stopped sending.\n";
	pthread_exit(NULL);
}

//ZAMYKANIE
void exiting(int signo) {
	for (int j = 0; j < maxNumberOfPlayers; j++) {
		cout<<"Closing thread "<<j<<".\n";
		pthread_join(mainHandles[j],NULL);
	}
	cout<<"Closing main socket.\n";
	close(mainSocket);
	cout<<"Exiting..."<<endl;
	exit(0);
}

//UTWORZENIE W�TK�W OBS�UGUJ�CYCH GRACZA I POSPRZ�TANIE PO NICH
void* serve(void* data) {
	char recvbuf[32] = "";
	int bytesRecv;

	bytesRecv = receive('\n', ((necessaryData*)data)->sock, recvbuf, 32);
	cout << recvbuf;
	bool t = true;

	if (findByName(recvbuf) == NULL) {
		send(*((necessaryData*)data)->sock, "OK\n", strlen("OK\n"), 0);
		if (bytesRecv > 0 && t) {
			((necessaryData*)data)->m = new Miasto(string(recvbuf));
			nd[((necessaryData*)data)->id] = (necessaryData*)data;

			cout<<*(((necessaryData*)data)->sock)<<endl;

			broadcastName(recvbuf, ((necessaryData*)data)->id, (necessaryData*)data);

			pthread_t handles[2];
			pthread_create(&handles[0], NULL, receiveMess, data);
			pthread_create(&handles[1], NULL, sendMess, data);

			int s=0;
			int i=0;
			while(true){
				s=pthread_tryjoin_np(handles[i],NULL);
				if(s==0)
					break;
				i=(i+1)%2;
				if(i==1)
					sleep(1);
			}

			nd[((necessaryData*)data)->id]->flag = false;
			for(int i=0 ; i<2 ; i++){
				s=pthread_tryjoin_np(handles[i],NULL);
				if(s==0)
					break;
			}
			delete ((necessaryData*)data)->m;
			((necessaryData*)data)->m = NULL;
		}
	}
	close(*((necessaryData*)data)->sock);
	cout << "Client " << ((necessaryData*)data)->id << " with socket (" << *(nd[((necessaryData*)data)->id]->sock) << ") disconnected" << endl;
	*((necessaryData*)data)->sock = -1;
	pthread_exit(NULL);
}

//INICJALIZACJA SOCKET�W
int initializeSockets(const char* ip, int port) {
	int mainSocket = socket(AF_INET, SOCK_STREAM, 0);
	if (mainSocket == -1) {
		cout << "Error creating socket: " << errno;
		return -1;
	}
	int on = 1;
	int ret = setsockopt( mainSocket, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on) );
	if(ret==-1){
		cout<<"Error setting socket options: "<< errno<<endl;
		return -1;
	}

	sockaddr_in service;
	memset(&service, 0, sizeof(service));
	service.sin_family = AF_INET;
	service.sin_addr.s_addr = inet_addr(ip);
	service.sin_port = htons(port);

	int r = bind(mainSocket, (struct sockaddr*)&service, sizeof(service));
	if (r == -1) {
		cout << "Binding failed.\n";
		close(mainSocket);
		return -1;
	}

	r = listen(mainSocket,maxNumberOfPlayers);
	if (r == -1) {
		cout << "Listening failed.\n";
		close(mainSocket);
		return -1;
	}

	return mainSocket;
}

void exiting2(){
	exiting(0);
}

int main() {
	atexit(exiting2);
	at_quick_exit(exiting2);
	signal(SIGTERM,exiting);
	signal(SIGINT,exiting);	
	signal(SIGHUP,exiting);
	signal(SIGQUIT,exiting);

	string ip;
	int port;
	cout << "\tCzy chcesz korzystac z adresu 127.0.0.1 i portu 27015 [t\\n]?\n";
	char yn = (char)getchar();
	while (getchar() != '\n');
	if (yn == 't') {
		ip = "127.0.0.1";
		port = 27015;
	}
	else {
		if (yn != 'n')cout << "Nie rozumiem co napisales, przyjmuje ze bylo to: 'nie'\n";
		cout << "IP:";
		cin >> ip;
		cout << "Port:";
		cin >> port;
	}
	
	//INICJALIZACJA ZMIENNYCH
	mainSocket = initializeSockets(ip.c_str(), port);

	if (mainSocket == -1) exit(-1);

	int** acceptSocket = new int*[maxNumberOfPlayers];
	
	int i = 0;

	for (int i = 0; i < maxNumberOfPlayers; i++) {
		acceptSocket[i] = new int;
		*(acceptSocket[i]) = -1;
	}
	nd = new necessaryData*[maxNumberOfPlayers];
	for (int j = 0; j < maxNumberOfPlayers; j++) nd[j] = new necessaryData(j);

	//��CZENIE SI� Z KLIENTAMI
	while (1) { 
		if (*(acceptSocket[i]) != -1) { 
			//cout << "Waiting for a client " << i << " to disconnect...\n";
			sleep(1); 
			i = (i + 1) % maxNumberOfPlayers;
			continue;
		}
		cout << "Waiting for a client " << i << " to connect...\n";
		while (*(acceptSocket[i]) == -1) {
			*(acceptSocket[i]) = accept(mainSocket, NULL, NULL);
		}
		cout << "Client connected.\n";

		pthread_join(mainHandles[i],NULL);
		necessaryData* t = new necessaryData(i, acceptSocket[i]);
		pthread_create(&mainHandles[i],NULL,serve,(void*) t);
		i=(i+1)%maxNumberOfPlayers;
	}
}
