#include "Stan.h"

using namespace std;

namespace patch
{
    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }
}

Stan::Stan(string nazwa)
{
	this->nazwa = nazwa;
	punkty = 0;

	robotnicy = 0;
	l_piechota = 0;
	c_piechota = 0;
	jazda = 0;

	surowce = 300;

	t = true;
}


Stan::~Stan()
{
}

char* Stan::toString()
{
	string s = patch::to_string(punkty)
		+ " " + patch::to_string(surowce) 
		+ " " + patch::to_string(robotnicy) 
		+ " " + patch::to_string(l_piechota) 
		+ " " + patch::to_string(c_piechota) 
		+ " " + patch::to_string(jazda)
		+ "\n";
	char *cstr = new char[s.length() + 1];
	strcpy(cstr, s.c_str());
	return cstr;
}
